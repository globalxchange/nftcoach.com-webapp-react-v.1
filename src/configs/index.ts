import fullLogo from "../static/images/appSpecific/fullLogo.svg";
import { ReactComponent as FullLogo } from "../static/images/appSpecific/fullLogo.svg";
import fullLogoTwo from "../static/images/appSpecific/fullLogoTwo.svg";
import logoIcon from "../static/images/appSpecific/logoIcon.svg";
import { ReactComponent as LogoIcon } from "../static/images/appSpecific/logoIcon.svg";

export const FULL_LOGO = fullLogo;
export const FULL_LOGO_TWO = fullLogoTwo;
export const LOGO_ICON = logoIcon;
export const APP_NAME = "IndianOTC";
export const APP_CODE = "indianotc";
export const DISP_CURRENCY = "INR";
export const DISP_CURRENCY_SYM = "₹";
export const TERMINAL_ID = "fdf793de-c110-4c99-b789-7eeb1cd121cd";
export const FullLogoSvg = FullLogo;
export const LogoIconSVG = LogoIcon;
