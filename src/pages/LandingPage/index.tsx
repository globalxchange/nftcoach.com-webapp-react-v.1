import { FULL_LOGO } from "../../configs";
import classNames from "./landingPage.module.scss";
import doubleArrowWhite from "../../static/images/icons/doubleArrowWhite.svg";

function LandingPage(): JSX.Element {
  return (
    <div className={classNames.landingPage}>
      <img src={FULL_LOGO} alt="" className={classNames.logo} />
      <div className={classNames.subtitle}>
        The Worlds First NFT Coaching Community
      </div>
      <label className={classNames.searchWrap}>
        <input
          type="text"
          className={classNames.input}
          placeholder="Enter Email To Join Waitlist"
        />
        <div className={classNames.btnGo}>
          <img src={doubleArrowWhite} alt="" className={classNames.icon} />
        </div>
      </label>
    </div>
  );
}

export default LandingPage;
